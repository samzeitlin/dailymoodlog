from django.contrib import admin

from .models import DailyLog, Thought, ThoughtList, Emotion, EmotionsChecklist, Distortion, DistortionsChecklist

class DailyLogAdmin(admin.ModelAdmin):
    list_display = ('pub_date',)

admin.site.register(DailyLog, DailyLogAdmin)

class ThoughtAdmin(admin.ModelAdmin):
    list_display = ('thought_text', 'thought_rating_before', 'thought_rating_after')
    list_editable = ('thought_text', 'thought_rating_before', 'thought_rating_after')

admin.site.register(Thought, ThoughtAdmin)

class EmotionAdmin(admin.ModelAdmin):
    list_display = ('emotion_text', 'emotion_rating_before', 'emotion_rating_after')
    list_editable = ('emotion_rating_before', 'emotion_rating_after')
    ordering = ('emotion_text',)

admin.site.register(Emotion, EmotionAdmin)

class DistortionAdmin(admin.ModelAdmin):
    list_display = ('distortion_text',)

admin.site.register(Distortion, DistortionAdmin)


#login is szeitlin, password is p2ssw0rd
