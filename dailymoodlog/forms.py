__author__ = 'szeitlin'

from django import forms
from django.forms.widgets import CheckboxInput
from dailymoodlog.models import User, DailyLog, Emotion, Thought, Distortion

from django.contrib.auth.forms import UserCreationForm
from django.utils import timezone

class LogForm(forms.ModelForm):

    today = timezone.now().date()
    new = DailyLog(pub_date = today)

    class Meta:
        model = DailyLog
        exclude = ['pub_date', 'loglist']

class UserForm(UserCreationForm):

    class Meta:
        model = User
        fields = ['username', 'email', 'password']


# class oldEmotionForm(forms.ModelForm):
#     #not using this
#     emotion_label = [item.emotion_text for item in Emotion.objects.all()]
#
#     #emotions = forms.ChoiceField(choices=Emotion.EMOTION_CHOICES, widget=CheckboxInput)
#     #emotion_choices = forms.models.ModelMultipleChoiceField(queryset = Emotion.objects.all(), widget=forms.CheckboxSelectMultiple())
#
#     class Meta:
#         model = Emotion
#         fields = ['emotion_text','checked_emotion', 'emotion_rating_before'] #, 'emotion_rating_after']
#         widgets= {'checked_emotion': CheckboxInput}
#          #note that this now has to be pre-populated and convert these to required=false


class ThoughtBeforeForm(forms.ModelForm):

    distortion_choices = forms.models.ModelMultipleChoiceField(queryset = Distortion.objects.all(), widget=forms.CheckboxSelectMultiple())

    class Meta:
        model = Thought
        fields = ['thought_text', 'thought_rating_before']


class EmotionAfterForm(forms.ModelForm):
    #not using this
    #want to limit to only those that were checked on the 'before' form

    emotion_choices = Emotion.objects.filter(checked_emotion = True)

    class Meta:
        model = Emotion
        fields = ['emotion_text','emotion_rating_after']

class ThoughtAfterForm(forms.ModelForm):

    #This one is for PositiveThoughtView, so it creates another set of thoughts

    class Meta:
        model = Thought
        fields = ['thought_text', 'thought_rating_after']


#https://github.com/pinax/django-forms-bootstrap

#https://github.com/pinax/pinax-blog/blob/master/pinax/blog/forms.py

#eventually will want this too: https://github.com/pinax/django-user-accounts

# import django_filters
#
# class EmotionFilter(django_filters.FilterSet):
#     class Meta:
#         model = Emotion
#         fields = ['emotion_text', 'emotion_rating_after']