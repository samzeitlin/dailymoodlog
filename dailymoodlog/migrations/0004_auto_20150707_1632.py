# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0003_auto_20150701_0833'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateTimeField(verbose_name=datetime.datetime(2015, 7, 7, 23, 32, 55, 98202, tzinfo=utc)),
        ),
    ]
