# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.utils.timezone import utc
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0004_auto_20150707_1632'),
    ]

    operations = [
        migrations.CreateModel(
            name='Emotions',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('checked_these', models.NullBooleanField()),
            ],
        ),
        migrations.CreateModel(
            name='EmotionsChecklist',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
            ],
        ),
        migrations.CreateModel(
            name='PercentRating',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
                ('rating', models.PositiveSmallIntegerField()),
            ],
        ),
        migrations.CreateModel(
            name='ThoughtList',
            fields=[
                ('id', models.AutoField(primary_key=True, serialize=False, verbose_name='ID', auto_created=True)),
            ],
        ),
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateField(verbose_name=datetime.datetime(2015, 7, 8, 0, 33, 17, 163366, tzinfo=utc)),
        ),
        migrations.AddField(
            model_name='emotions',
            name='emotionlist',
            field=models.ForeignKey(default=None, to='dailymoodlog.EmotionsChecklist'),
        ),
        migrations.AddField(
            model_name='thought',
            name='thoughtlist',
            field=models.ForeignKey(default=None, to='dailymoodlog.ThoughtList'),
        ),
    ]
