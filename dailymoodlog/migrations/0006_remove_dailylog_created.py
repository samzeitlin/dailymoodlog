# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0005_auto_20150707_1733'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dailylog',
            name='created',
        ),
    ]
