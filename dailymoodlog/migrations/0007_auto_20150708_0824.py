# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0006_remove_dailylog_created'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emotions',
            name='emotionlist',
            field=models.ForeignKey(to='dailymoodlog.EmotionsChecklist'),
        ),
        migrations.AlterField(
            model_name='thought',
            name='thoughtlist',
            field=models.ForeignKey(to='dailymoodlog.ThoughtList'),
        ),
    ]
