# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0009_auto_20150709_1012'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailylog',
            name='published_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='distortion',
            name='checked_distortion',
            field=models.NullBooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='checked_emotion',
            field=models.NullBooleanField(default=False),
        ),
    ]
