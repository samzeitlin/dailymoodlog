# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0010_auto_20150709_1238'),
    ]

    operations = [
        migrations.AlterField(
            model_name='distortion',
            name='distortionlist',
            field=models.ForeignKey(to='dailymoodlog.DistortionsChecklist'),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotionlist',
            field=models.ForeignKey(to='dailymoodlog.EmotionsChecklist'),
        ),
        migrations.AlterField(
            model_name='thought',
            name='thoughtlist',
            field=models.ForeignKey(to='dailymoodlog.ThoughtList'),
        ),
    ]
