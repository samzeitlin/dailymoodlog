# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations

def populate_emotions(apps, schema_editor):
    Emotion = apps.get_model("dailymoodlog", "Emotion")

    EMOTION_CHOICES = ('Sad', 'depressed', 'unhappy',
     'Anxious', 'worried', 'panicky', 'nervous', 'scared',
     'Guilty', 'ashamed',
     'Inferior', 'worthless', 'inadequate', 'defective', 'incompetent',
     'Lonely', 'unloved', 'unwanted', 'rejected', 'alone', 'abandoned', 'isolated',
     'Embarassed', 'foolish', 'humiliated', 'self-conscious', 'awkward', 'clumsy', 'ham-handed',
     'Hopeless', 'discouraged', 'pessimistic', 'despairing',
     'Frustrated', 'stuck', 'defeated', 'confused', 'unmoored', 'lost',
     'Angry','mad', 'resentful', 'annoyed', 'irritated', 'upset', 'furious', 'aggravated')

    for emotion in EMOTION_CHOICES:
        Emotion(emotion_text ="%s" %(emotion)).save()

def populate_distortions(apps, schema_editor):
    Distortion = apps.get_model("dailymoodlog", "Distortion")

    DISTORTION_CHOICES = ("all-or-nothing",
    "overgeneralization",
    "mental filter: dwelling on the negative",
    "discounting the positives",
    "jumping to conclusions",
    "mind-reading",
    "fortune-telling",
    "magnification",
    "emotional reasoning",
    "should statements",
    "labeling yourself",
    "taking things too personally",
    "blaming yourself")

    for distortion in DISTORTION_CHOICES:
        Distortion(distortion_text= "%s" %(distortion)).save()

class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0012_auto_20150709_1256'),
    ]

    operations = [
        migrations.RunPython(populate_emotions),
        migrations.RunPython(populate_distortions),
    ]
