# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0013_auto_20150710_1600'),
    ]

    operations = [
        migrations.RenameField(
            model_name='emotion',
            old_name='emotionrating_after',
            new_name='emotion_rating_after',
        ),
        migrations.RenameField(
            model_name='emotion',
            old_name='emotionrating_before',
            new_name='emotion_rating_before',
        ),
        migrations.RenameField(
            model_name='thought',
            old_name='thoughtrating_after',
            new_name='thought_rating_after',
        ),
        migrations.RenameField(
            model_name='thought',
            old_name='thoughtrating_before',
            new_name='thought_rating_before',
        ),
        migrations.RemoveField(
            model_name='distortion',
            name='distortionlist',
        ),
        migrations.RemoveField(
            model_name='emotion',
            name='checked_emotion',
        ),
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateField(default=datetime.date(2015, 7, 12)),
        ),
        migrations.AlterField(
            model_name='distortion',
            name='checked_distortion',
            field=models.ForeignKey(to='dailymoodlog.DistortionsChecklist', null=True, blank=True),
        ),
    ]
