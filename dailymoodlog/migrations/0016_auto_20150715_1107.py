# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0015_auto_20150713_0911'),
    ]

    operations = [
        migrations.AddField(
            model_name='thought',
            name='relevant_distortions',
            field=models.ForeignKey(to='dailymoodlog.DistortionsChecklist', null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateField(default=datetime.date(2015, 7, 15)),
        ),
    ]
