# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0019_auto_20150720_1714'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='dailyloglist',
            name='pub_date',
        ),
        migrations.RemoveField(
            model_name='emotion',
            name='pub_date',
        ),
        migrations.RemoveField(
            model_name='thought',
            name='pub_date',
        ),
        migrations.AddField(
            model_name='dailylog',
            name='loglist',
            field=models.ForeignKey(null=True, to='dailymoodlog.DailyLogList', blank=True),
        ),
        migrations.AddField(
            model_name='emotion',
            name='created',
            field=models.ForeignKey(null=True, to='dailymoodlog.DailyLog', blank=True),
        ),
        migrations.AddField(
            model_name='thought',
            name='created',
            field=models.ForeignKey(null=True, to='dailymoodlog.DailyLog', blank=True),
        ),
    ]
