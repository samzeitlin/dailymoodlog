# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0020_auto_20150720_1742'),
    ]

    operations = [
        migrations.RenameField(
            model_name='emotion',
            old_name='created',
            new_name='dailylog',
        ),
        migrations.RenameField(
            model_name='thought',
            old_name='created',
            new_name='dailylog',
        ),
        migrations.AddField(
            model_name='distortion',
            name='dailylog',
            field=models.ForeignKey(to='dailymoodlog.DailyLog', blank=True, null=True),
        ),
    ]
