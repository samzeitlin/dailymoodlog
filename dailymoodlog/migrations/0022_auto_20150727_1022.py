# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0021_auto_20150720_1810'),
    ]

    operations = [
        migrations.AddField(
            model_name='dailylog',
            name='pub_date',
            field=models.DateField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateField(default=datetime.date(2015, 7, 27)),
        ),
    ]
