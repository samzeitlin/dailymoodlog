# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0022_auto_20150727_1022'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='distortion',
            name='id',
        ),
        migrations.RemoveField(
            model_name='emotion',
            name='id',
        ),
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateField(default=datetime.date(2015, 7, 29)),
        ),
        migrations.AlterField(
            model_name='distortion',
            name='distortion_text',
            field=models.TextField(serialize=False, default='', primary_key=True, max_length=200),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotion_text',
            field=models.CharField(serialize=False, default='', primary_key=True, max_length=250),
        ),
    ]
