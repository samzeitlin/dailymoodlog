# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0023_auto_20150729_0952'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailylog',
            name='created',
            field=models.DateField(default=datetime.date(2015, 7, 31)),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotion_rating_after',
            field=models.IntegerField(blank=True, max_length=3, null=True),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotion_rating_before',
            field=models.IntegerField(blank=True, max_length=3, null=True),
        ),
    ]
