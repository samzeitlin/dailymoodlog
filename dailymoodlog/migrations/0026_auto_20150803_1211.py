# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0025_auto_20150803_1057'),
    ]

    operations = [
        migrations.AlterField(
            model_name='emotion',
            name='emotion_rating_after',
            field=models.IntegerField(null=True, blank=True),
        ),
        migrations.AlterField(
            model_name='emotion',
            name='emotion_rating_before',
            field=models.IntegerField(null=True, blank=True),
        ),
    ]
