# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import datetime


class Migration(migrations.Migration):

    dependencies = [
        ('dailymoodlog', '0026_auto_20150803_1211'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dailylog',
            name='pub_date',
            field=models.DateField(blank=True, default=datetime.date(2015, 8, 3), null=True),
        ),
    ]
