from django.db import models
from django.utils import timezone
from datetime import datetime
import uuid

from django.contrib.auth.models import User


class DailyLogList(models.Model):

    def __str__(self):
        pass

class DailyLog(models.Model):

    #primary key is automatically generated id number

    today = timezone.now().date()
    created = models.DateField(default = today)
    pub_date = models.DateField(blank=True, null=True, default=today)
    loglist = models.ForeignKey(DailyLogList, null=True, blank=True)

    #also want to associate username with this

    def __str__(self):
        """
        :return: date converted to a string
        """
        self.created_date = datetime.strftime(self.today, '%m/%d/%Y')
        self.first_published = datetime.strftime(self.pub_date, '%m/%d/%Y')
        return self.first_published

class DistortionsChecklist(models.Model):
    """    populated the list of distortions into the db w/a custom migration (0013)
           pull them back out with a query set
    """
    #relevant_distortions = Distortion.objects.filter(distortion_choices = True)

    def __str__(self):
        pass


class Distortion(models.Model):
    """ Distortions are identified, but not rated. """

    distortion_id = models.AutoField(primary_key=True)
    distortion_text = models.TextField(max_length=200, default='not working correctly') # think we can use auto-id instead of (primary_key=True)
    checked_distortion = models.NullBooleanField(default=False)
    distortionlist = models.ForeignKey(DistortionsChecklist, null=True, blank=True)
    dailylog = models.ForeignKey(DailyLog, null=True, blank=True)

    def __str__(self):
        return self.distortion_text


class ThoughtList(models.Model):

    def __str__(self):
        pass

class Thought(models.Model):
    """ There will be two sets: negative thoughts + %rating before and after,
    and positive thoughts + %rating (will show up as 'belief' on the page)"""

    thought_text = models.CharField(max_length=250, default='')
    thoughtlist = models.ForeignKey(ThoughtList, null=True, blank=True)

    relevant_distortions = models.ForeignKey(DistortionsChecklist, null=True, blank=True)

    thought_rating_before = models.IntegerField(default=50)
    thought_rating_after = models.IntegerField(default=50)

    dailylog = models.ForeignKey(DailyLog, null=True, blank=True)

    def __str__(self):
        return self.thought_text #, pub_date


class EmotionsChecklist(models.Model):

    #checked_emotions = #might want this to be an arrayfield or something similar? which postgres should support but sqlite does not?

    def __str__(self):
        pass
        # for item in self.checked_emotions:
        #     return item.emotion_text


class Emotion(models.Model):
    """user can choose more than one, will connect with CheckboxInput widget"""

    EMOTION_CHOICES = (
        ('Sad', (
            ('Sad', 'Sad'),
            ('depressed', 'depressed'),
            ('unhappy','unhappy')
        )),
         ('Anxious', (
             ('Anxious','Anxious'),
             ('worried','worried'),
             ('panicky','panicky'),
             ('nervous','nervous'),
             ('scared', 'scared')
         )),

          ('Guilty', (
              ('Guilty', 'Guilty'),
              ('ashamed','ashamed')
          )),
           ('Inferior', (
               ('Inferior', 'Inferior'),
               ('worthless', 'worthless'),
               ('inadequate', 'inadequate'),
               ('defective', 'defective'),
               ('incompetent','incompetent')
           )),
            ('Lonely', (
                ('Lonely','Lonely'),
                ('unloved','unloved'),
                ('unwanted', 'unwanted'),
                ('rejected', 'rejected'),
                ('alone', 'alone'),
                ('abandoned', 'abandoned'),
                ('isolated','isolated')
            )),
             ('Embarassed', (
                 ('Embarassed','Embarassed'),
                 ('foolish','foolish'),
                 ('humiliated', 'humiliated'),
                 ('self-conscious','self-conscious'),
                 ('awkward', 'awkward'),
                 ('clumsy', 'clumsy'),
                 ('ham-handed','ham-handed')
             )),
              ('Hopeless', (
                  ('Hopeless','Hopeless'),
                  ('discouraged', 'discouraged'),
                  ('pessimistic', 'pessimistic'),
                  ('despairing','despairing')
              )),
               ('Frustrated', (
                   ('Frustrated','Frustrated'),
                   ('stuck', 'stuck'),
                   ('defeated', 'defeated'),
                   ('confused', 'confused'),
                   ('unmoored', 'unmoored'),
                   ('lost','lost')
               )),
                ('Angry',(
                    ('Angry', 'Angry'),
                    ('mad', 'mad'),
                    ('resentful', 'resentful'),
                    ('annoyed', 'annoyed'),
                    ('irritated','irritated'),
                    ('upset', 'upset'),
                    ('furious', 'furious'),
                    ('aggravated','aggravated')
                ))
                 )

# was toying with making the emotion_text the primary key, but I think that will be more troublesome w/unique constraints
# figured out how to add it to row_id with jQuery, which should be ok instead

    emotion_id = models.AutoField(primary_key=True)
    emotion_text = models.CharField(max_length=250, default='not working correctly') #primary_key=True) #, choices = EMOTION_CHOICES,)
    checked_emotion = models.NullBooleanField(default=False)
    emotionlist = models.ForeignKey(EmotionsChecklist, null=True, blank=True)

    emotion_rating_before = models.IntegerField(null=True, blank=True)
    emotion_rating_after = models.IntegerField(null=True, blank=True)

    dailylog = models.ForeignKey(DailyLog, null=True, blank=True)

    def __str__(self):
        return self.emotion_text


# import django_filters
#
# class EmotionFilter(django_filters.FilterSet):
#     class Meta:
#         model = Emotion
#         fields = ['emotion_text', 'checked_emotion', 'emotion_rating_before']



