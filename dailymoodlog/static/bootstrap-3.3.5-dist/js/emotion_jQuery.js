/**
 * Created by szeitlin on 7/29/15.
 */

jQuery(document).ready(function() {

    function getCookie(name) {
        var cookieValue = null;
        if (document.cookie && document.cookie != '') {
            var cookies = document.cookie.split(';');
            for (var i = 0; i < cookies.length; i++) {
                var cookie = jQuery.trim(cookies[i]);
                // Does this cookie string begin with the name we want?
                if (cookie.substring(0, name.length + 1) == (name + '=')) {
                    cookieValue = decodeURIComponent(cookie.substring(name.length + 1));
                    break;
                }
            }
        }
        return cookieValue;
    }

    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", getCookie('csrftoken'));
            }
        }
    });


    //want to use emotion name as row_id
    // take list of emotion names from one column and add them to the row

    var arrList = $("tr > td.checked_emotion input").each(function () {
        $(this).val();
    });


    //this works - be careful b/c the header row is also tr
    $.each($("tbody tr"), function (i) {
        $(this).attr("row_id", arrList[i]['value']);
    });



     var feeling = jQuery("td.checked_emotion");

    feeling.on("click", function (event) {

            var inputFieldNode = [jQuery("input:checked").val(),

                jQuery("td.emotion_rating_before input").val()];

     console.log(event.type);
     console.log(inputFieldNode);
     });
     });



    /* var rating = jQuery("td.emotion_rating_before input");

    var feelingsList = feeling.map(function () {
        return this.val;
    }).toArray();
    var ratingsList = rating.map(function () {
        return this.val;
    }).toArray();

    var submitList = jQuery("button");

    submitList.on("click", function (event) {

        if (event.type == 'click') {
            alert(feelingsList, ratingsList);

            jQuery.ajax({

                url: "/api/v1/?format=json",
                data: JSON.stringify({
                    checked_emotion: feelingsList,
                    emotion_rating_before: ratingsList
                }),
                type: "POST"

            })

                .done(function (response) {
                    jQuery(this).addClass("response");

                    if ((typeof response != 'undefined') && response) {
                        console.log("success: " + event);
                    }
                    else {
                        console.log(xhr.status + ": " + xhr.responseText);
                    }
                });
        }
    });

});
*/








