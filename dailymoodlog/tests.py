from django.test import TestCase
from django.core.urlresolvers import resolve
from django.http import HttpRequest
from django.utils import timezone

from .views import HomeView, get_emotions, CreateThoughtView

from dailymoodlog.models import DailyLog, Thought, Emotion, Distortion
import dailymoodlog

class HomePageTest(TestCase):

    def test_root_url_status_code(self):
        response = self.client.get('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'dailymoodlog/base.html')

    def test_root_url_resolves_to_home_page_view(self):
        found = resolve('/')
        self.assertEqual(found.func, dailymoodlog.views.HomeView)

    def test_home_page_returns_correct_html(self):
        request = HttpRequest()
        response = HomeView(request)
        self.assertTrue(response.content.startswith(b'<html>'))
        self.assertIn(b'<title>Daily Mood Log</title>', response.content)
        self.assertTrue(response.content.endswith(b'</html>'))

class DailyLogTests(TestCase):

    def test_db_can_create_and_return_a_log(self):
        d = DailyLog()
        self.assertEqual(d.created, timezone.now().date())



class EmotionTests(TestCase):

    def test_can_choose_and_return_an_emotion(self):
        e = Emotion.objects.get(emotion_text='Sad')
        self.assertEqual(e.emotion_text, 'Sad')

class ThoughtTests(TestCase):

    def test_can_create_and_return_a_thought(self):
        t = Thought(thought_text="I'm afraid of job interviews")
        self.assertEqual(t.thought_text, "I'm afraid of job interviews")


class DistortionTests(TestCase):

    def test_can_choose_and_return_an_existing_distortion(self):
        d=Distortion.objects.get(distortion_text='all-or-nothing')
        self.assertEqual(d.distortion_text, 'all-or-nothing')

    def test_cannot_choose_and_return_a_nonexisting_distortion(self):
        d = Distortion.objects.get(distortion_text='self-immolation')
        self.assertEqual(d.distortion_text, 'self-immolation')


